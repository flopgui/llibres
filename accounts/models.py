from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    email = models.EmailField(max_length=254, unique=True, blank=False)
    first_name = models.CharField(max_length=30, blank=False)
