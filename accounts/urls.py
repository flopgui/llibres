from django.urls import path

from . import views

app_name = 'accounts'
urlpatterns = [
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('edit/<int:pk>', views.AccountUpdate.as_view(), name='account_update'),
    path('change_password', views.change_password, name='change_password'),
]
