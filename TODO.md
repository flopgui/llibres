### TODO list

- [X] PostgreSQL
- [X] Search function
- [X] Pagination
- [X] Better shelf system
- [X] Profile
- [ ] Tests
- [ ] Main page
- [ ] Library management
- [ ] Password recovery
- [ ] Internationalization
- [ ] Better theme
- [ ] Tags
- [ ] Settings
- [ ] Export library
- [ ] Deployment
