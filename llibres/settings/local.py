from llibres.settings.common import *
from llibres.settings.secrets import *

DEBUG = True
INSTALLED_APPS += ['debug_toolbar']
MIDDLEWARE.insert(0, 'debug_toolbar.middleware.DebugToolbarMiddleware')

ALLOWED_HOSTS = ['localhost', '192.168.1.40']
INTERNAL_IPS = ['192.168.1.40', ]
