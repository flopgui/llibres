from llibres.settings.common import *
import dj_database_url
import os

DEBUG = False
ALLOWED_HOSTS = ['.herokuapp.com']
DATABASES = {'default': dj_database_url.parse(os.getenv('DATABASE_URL'))}
MIDDLEWARE.insert(0, 'whitenoise.middleware.WhiteNoiseMiddleware')
STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
