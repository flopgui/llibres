from django.contrib import admin
from .models import Library, Shelf, Book, BookCopy

for i in [Library, Shelf, Book, BookCopy]:
    admin.site.register(i)
