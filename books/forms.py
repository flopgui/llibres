from django import forms


class NewLibraryForm(forms.Form):
    name = forms.CharField(label='Name', max_length=30, widget=forms.TextInput(
        attrs={'class': 'input', 'placeholder': 'Name'}))


class NewBookForm(forms.Form):
    isbn = forms.DecimalField(label='ISBN', min_value=1, max_digits=13, decimal_places=0)

    def __init__(self, *args, **kwargs):
        shelf_list = kwargs.pop('shelf_list')
        super(NewBookForm, self).__init__(*args, **kwargs)
        self.fields['location'] = forms.TypedChoiceField(label='Shelf', choices=shelf_list, coerce=int)
        self.fields['isbn'].widget = forms.NumberInput(
            attrs={'id': 'isbn-input', 'class': 'input', 'placeholder': 'ISBN'})


class NewShelfForm(forms.Form):
    name = forms.CharField(label='Name', max_length=30,
                           widget=forms.TextInput(attrs={'class': 'input', 'placeholder': 'Name'}))
