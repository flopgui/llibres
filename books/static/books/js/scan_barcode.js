window.onload = function() {
    infile = document.getElementById("input-file");
    code_output = document.getElementById("isbn-input");

    infile.onchange = function() {
        Quagga.decodeSingle({
            decoder: {
                readers: ['ean_reader', 'ean_8_reader']
            },
            locate: true,
            src: URL.createObjectURL(infile.files[0])
        }, function(result){
            if(result.codeResult) {
                code_output.value = result.codeResult.code;
            } else {
                code_output.value = "-1";
                alert("not detected");
            }
        });
    }
}
