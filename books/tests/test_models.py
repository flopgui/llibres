from django.test import TestCase
from django.core.exceptions import ValidationError
from model_bakery import baker

from books.models import Book, BookCopy, Library, Shelf


class TestModels(TestCase):

    def test_str_conversions(self):
        library = baker.make(Library, name="My library")
        shelf = baker.make(Shelf, name="My shelf", library=library)
        book = baker.make(Book, isbn13="9788478446483", title="EL DIMONI DELS NOMBRES",
                          authors=["Enzensberger, Hans Magnus"])
        book_copy = baker.make(BookCopy, location=shelf, book=book)
        library.save()
        shelf.save()
        book.save()
        book_copy.save()
        self.assertEqual(str(library), "My library")
        self.assertEqual(str(shelf), "My shelf at My library")
        self.assertEqual(str(book), "EL DIMONI DELS NOMBRES (9788478446483)")
        self.assertEqual(str(book_copy), "EL DIMONI DELS NOMBRES (9788478446483) at My shelf, My library")

    def test_isbn_validation(self):
        book = baker.make(Book, isbn13="-1", authors=["You"])  # Negative number
        self.assertRaises(ValidationError, book.full_clean)
        book = baker.make(Book, isbn13="9782", authors=["You"])  # Too short
        self.assertRaises(ValidationError, book.full_clean)
        book = baker.make(Book, isbn13="1234567890128", authors=["You"])  # Doesn't start with 978 or 979
        self.assertRaises(ValidationError, book.full_clean)
        book = baker.make(Book, isbn13="9781234567896", authors=["You"])  # Incorrect check digit
        self.assertRaises(ValidationError, book.full_clean)
        book = baker.make(Book, isbn13="9781234567897", authors=["You"])
        book.full_clean()
        book = baker.make(Book, isbn13="9791234567896", authors=["You"])
        book.full_clean()

    def test_book_api_lookups(self):
        library = baker.make(Library, name="My library")
        shelf = baker.make(Shelf, name="My shelf", library=library)
        book = BookCopy.create("9788478446483", shelf)
        self.assertTrue(book.book.title.lower().startswith("el dimoni dels nombres"))
        self.assertTrue("enzensberger" in book.book.authors[0].lower())
        self.assertEquals(book.location.name, "My shelf")

    def test_authors_management(self):
        book = baker.make(Book, authors=["March, Ausiàs"])
        self.assertEquals(book.authors, ["March, Ausiàs"])
        book = baker.make(Book, authors=["March, Ausiàs", "Llull, Ramon"])
        self.assertEquals(book.authors, ["March, Ausiàs", "Llull, Ramon"])
        book = baker.make(Book, authors=['["I want to fool you", "Did I?"]'])
        self.assertEquals(book.authors, ['["I want to fool you", "Did I?"]'])
