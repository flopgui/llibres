from django.test import TestCase
from django.urls import reverse
from model_bakery import baker

from books.models import Book, BookCopy, Library, Shelf
from accounts.models import User


class TestLibrariesView(TestCase):

    def test_anonymous_cannot_see_libraries(self):
        response = self.client.get(reverse('books:libraries'))
        self.assertRedirects(response, reverse('login') + '?next=' + reverse('books:libraries'))
        response = self.client.post(reverse('books:libraries'), data={'name': 'My library'})
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(Library.objects.count(), 0)

    def test_can_add_library(self):
        user = User.objects.create_user("olentzero", "olentzero@olentzero.eus", "buru_handia")
        self.client.force_login(user=user)
        response = self.client.get(reverse("books:libraries"))
        self.assertEqual(response.status_code, 200)
        response = self.client.post(reverse("books:libraries"), data={'name': 'My library'})
        self.assertEqual(Library.objects.count(), 1)
        self.assertRedirects(response, reverse("books:libraries"))
        response = self.client.post(reverse("books:libraries"), data={'name': ''})
        self.assertEqual(Library.objects.count(), 1)


class TestLibraryDetailView(TestCase):

    def setUp(self):
        self.user = User.objects.create_user("olentzero", "olentzero@olentzero.eus", "buru_handia")
        self.library = baker.make(Library, name="My library", owner=self.user)
        self.library.save()
        self.shelf1 = baker.make(Shelf, library=self.library, name="S1")
        self.shelf2 = baker.make(Shelf, library=self.library, name="S2")
        self.shelf1.save()
        self.shelf2.save()
        for i in range(10):
            b = baker.make(Book, authors=["A"], title="B{}".format(str(i)*20))
            bc = baker.make(BookCopy, location=self.shelf1, book=b)
            bc.save()
        for i in range(5):
            b = baker.make(Book, authors=["A"], title="B{}".format(str(i)*20))
            bc = baker.make(BookCopy, location=self.shelf2, book=b)
            bc.save()

    def test_anonymous_cannot_see_library(self):
        url = reverse('books:library_detail', args=[self.library.pk])
        response = self.client.get(url)
        self.assertRedirects(response, reverse('login') + '?next=' + url)

    def test_not_owner_cannot_see_library(self):
        fake_user = User.objects.create_user("santaclaus", "santa@santa.fi", "hohoho")
        self.client.force_login(user=fake_user)
        url = reverse('books:library_detail', args=[self.library.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_can_see_library(self):
        self.client.force_login(user=self.user)
        url = reverse('books:library_detail', args=[self.library.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_can_search_library(self):
        self.client.force_login(user=self.user)
        url = reverse('books:library_detail', args=[self.library.pk])
        response = self.client.get(url, data={'q': '1'*20})
        self.assertEqual(len(response.context['book_list']), 2)

    def test_can_filter_by_shelf(self):
        self.client.force_login(user=self.user)
        url = reverse('books:library_detail', args=[self.library.pk])
        response = self.client.get(url, data={'shelf': self.shelf1.pk})
        self.assertEqual(len(response.context['book_list']), 10)
        response = self.client.get(url, data={'shelf': self.shelf1.pk, 'q': '1'*20})
        self.assertEqual(len(response.context['book_list']), 1)

    def test_can_order_library(self):
        self.client.force_login(user=self.user)
        url = reverse('books:library_detail', args=[self.library.pk])
        response = self.client.get(url, data={'ordering': 'isbn'})
        bl = response.context['book_list']
        for i in range(len(bl) - 1):
            self.assertTrue(bl[i].book.isbn13 <= bl[i+1].book.isbn13)
        response = self.client.get(url, data={'ordering': '-isbn'})
        bl = response.context['book_list']
        for i in range(len(bl) - 1):
            self.assertTrue(bl[i].book.isbn13 >= bl[i+1].book.isbn13)
        response = self.client.get(url, data={'ordering': 'isbn', 'shelf': self.shelf1.pk})
        bl = response.context['book_list']
        for i in range(len(bl) - 1):
            self.assertTrue(bl[i].book.isbn13 <= bl[i+1].book.isbn13)
        response = self.client.get(url, data={'ordering': 'isbn', 'q': '2'*i, 'shelf': ''})
        bl = response.context['book_list']
        for i in range(len(bl) - 1):
            self.assertTrue(bl[i].book.isbn13 <= bl[i+1].book.isbn13)

    def test_library_pagination(self):
        self.client.force_login(user=self.user)
        url = reverse('books:library_detail', args=[self.library.pk])
        response = self.client.get(url)
        self.assertEqual(response.context['page_obj'].number, 1)
        self.assertEqual(response.context['page_obj'].paginator.num_pages, 2)
        self.assertEqual(len(response.context['page_obj'].object_list), 10)
        response = self.client.get(url, data={'page_size': '4', 'page': '1'})
        self.assertEqual(response.context['page_obj'].number, 1)
        self.assertEqual(response.context['page_obj'].paginator.num_pages, 4)
        self.assertEqual(len(response.context['page_obj'].object_list), 4)
        response = self.client.get(url, data={'page_size': '4', 'page': '4'})
        self.assertEqual(response.context['page_obj'].number, 4)
        self.assertEqual(response.context['page_obj'].paginator.num_pages, 4)
        self.assertEqual(len(response.context['page_obj'].object_list), 3)


class TestBookAddView(TestCase):

    def setUp(self):
        self.user = User.objects.create_user("olentzero", "olentzero@olentzero.eus", "buru_handia")
        self.library = baker.make(Library, name="My library", owner=self.user)
        self.shelf = baker.make(Shelf, library=self.library, name="S1")

    def test_anonymous_cannot_add_book(self):
        url = reverse('books:book_add', args=[self.library.pk])
        response = self.client.post(url, data={'isbn': "9788478446483", 'location': self.shelf.pk})
        self.assertEqual(BookCopy.objects.count(), 0)
        self.assertRedirects(response, reverse('login') + '?next=' + url)

    def test_not_owner_cannot_add_book(self):
        fake_user = User.objects.create_user("santaclaus", "santa@santa.fi", "hohoho")
        self.client.force_login(user=fake_user)
        url = reverse('books:book_add', args=[self.library.pk])
        response = self.client.post(url, data={'isbn': "9788478446483", 'location': self.shelf.pk})
        self.assertEqual(BookCopy.objects.count(), 0)
        self.assertEqual(response.status_code, 403)

    def test_can_add_book(self):
        self.client.force_login(user=self.user)
        url = reverse('books:book_add', args=[self.library.pk])
        response = self.client.post(url, data={'isbn': "9788478446483", 'location': self.shelf.pk})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(BookCopy.objects.count(), 1)

    def test_cannot_add_book_to_empty_library(self):
        self.client.force_login(user=self.user)
        fake_library = baker.make(Library, name="Not a library", owner=self.user)
        url = reverse('books:book_add', args=[fake_library.pk])
        self.client.post(url, data={'isbn': "9788478446483", 'location': ''})
        self.assertEqual(BookCopy.objects.count(), 0)

    def test_cannot_add_book_to_other_library(self):
        self.client.force_login(user=self.user)
        fake_library = baker.make(Library, name="Not a library", owner=self.user)
        baker.make(Shelf, library=fake_library)
        url = reverse('books:book_add', args=[fake_library.pk])
        self.client.post(url, data={'isbn': "9788478446483", 'location': self.shelf.pk})
        self.assertEqual(BookCopy.objects.count(), 0)

    def test_cannot_add_book_to_not_existing_library(self):
        self.client.force_login(user=self.user)
        url = reverse('books:book_add', args=[self.library.pk + 1])
        response = self.client.post(url, data={'isbn': "9788478446483", 'location': self.shelf.pk})
        self.assertEqual(response.status_code, 404)


class TestShelfAddView(TestCase):

    def setUp(self):
        self.user = User.objects.create_user("olentzero", "olentzero@olentzero.eus", "buru_handia")
        self.library = baker.make(Library, name="My library", owner=self.user)

    def test_anonymous_cannot_add_shelf(self):
        url = reverse('books:shelf_add', args=[self.library.pk])
        response = self.client.post(url, data={'name': 'My shelf'})
        self.assertEqual(Shelf.objects.count(), 0)
        self.assertRedirects(response, reverse('login') + '?next=' + url)

    def test_not_owner_cannot_add_book(self):
        fake_user = User.objects.create_user("santaclaus", "santa@santa.fi", "hohoho")
        self.client.force_login(user=fake_user)
        url = reverse('books:shelf_add', args=[self.library.pk])
        response = self.client.post(url, data={'name': 'My shelf'})
        self.assertEqual(Shelf.objects.count(), 0)
        self.assertEqual(response.status_code, 403)

    def test_can_add_book(self):
        self.client.force_login(user=self.user)
        url = reverse('books:shelf_add', args=[self.library.pk])
        response = self.client.post(url, data={'name': 'My shelf'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Shelf.objects.count(), 1)
        response = self.client.post(url, data={'name': ''})
        self.assertEqual(Shelf.objects.count(), 1)

    def test_cannot_add_shelf_to_not_existing_library(self):
        self.client.force_login(user=self.user)
        url = reverse('books:shelf_add', args=[self.library.pk + 1])
        response = self.client.post(url, data={'name': 'My shelf'})
        self.assertEqual(response.status_code, 404)


class TestBookDetailView(TestCase):

    def test_check_book_copy_locator(self):
        user = User.objects.create_user("olentzero", "olentzero@olentzero.eus", "buru_handia")
        fake_user = User.objects.create_user("santaclaus", "santa@santa.fi", "hohoho")
        library1 = baker.make(Library, owner=user)
        library2 = baker.make(Library, owner=user)
        fake_library = baker.make(Library, owner=fake_user)
        shelf1 = baker.make(Shelf, library=library1)
        shelf2 = baker.make(Shelf, library=library1)
        shelf3 = baker.make(Shelf, library=library2)
        fake_shelf = baker.make(Shelf, library=fake_library)
        book = baker.make(Book, authors=['A'])
        baker.make(BookCopy, 10, book=book, location=shelf1)
        baker.make(BookCopy, book=book, location=shelf2)
        baker.make(BookCopy, book=book, location=shelf3)
        baker.make(BookCopy, 4, book=book, location=fake_shelf)
        self.client.force_login(user=user)
        url = reverse('books:book_detail', args=[book.pk])
        response = self.client.get(url)
        self.assertEqual(len(response.context['copy_list']), 12)
        self.assertEqual(len(response.context['counted_copy_list']), 3)
