from django.urls import path

from . import views

app_name = 'books'
urlpatterns = [
    path('', views.BooksIndex.as_view(), name='index'),
    path('libraries', views.LibrariesList.as_view(), name='libraries'),
    path('library/<int:pk>/', views.LibraryDetail.as_view(), name='library_detail'),
    path('library/<int:pk>/book_add', views.book_add, name='book_add'),
    path('library/<int:pk>/shelf_add', views.shelf_add, name='shelf_add'),
    path('book/<int:pk>/', views.BookDetail.as_view(), name='book_detail'),
    path('copy_delete/<int:pk>/', views.BookCopyDelete.as_view(), name='copy_delete'),
    path('copy_move/<int:pk>/', views.copy_move, name='copy_move'),
    path('shelf_delete/<int:pk>/', views.ShelfDelete.as_view(), name='shelf_delete'),
    path('shelf_rename/<int:pk>/', views.shelf_rename, name='shelf_rename'),
    path('book_update/<int:pk>/', views.BookUpdate.as_view(), name='book_update'),
]
