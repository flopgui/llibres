from django.core.exceptions import PermissionDenied, ValidationError
from django.core.paginator import Paginator
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.postgres.search import TrigramSimilarity
from django.http import HttpResponse, HttpResponseRedirect  # , Http404
from django.shortcuts import get_object_or_404  # , render
from django.urls import reverse
from django.views import generic
from urllib.parse import urlencode

from .models import Book, BookCopy, Library, Shelf, ISBNLookupFail
from .forms import NewLibraryForm, NewBookForm, NewShelfForm


class BooksIndex(generic.TemplateView):
    template_name = 'books/index.html'


class LibrariesList(LoginRequiredMixin, generic.ListView):
    template_name = 'books/libraries.html'
    context_object_name = 'libraries_list'

    def get_queryset(self):
        return Library.objects.filter(owner=self.request.user).order_by('name')

    def get_context_data(self, **kwargs):
        context = super(LibrariesList, self).get_context_data(**kwargs)
        context['form'] = NewLibraryForm()
        return context

    def post(self, request, *args, **kwargs):
        form = NewLibraryForm(request.POST)
        if form.is_valid():
            lib = Library(name=form.cleaned_data['name'], owner=request.user)
            try:
                lib.save()
                return HttpResponseRedirect(reverse('books:libraries', args=args, kwargs=kwargs))
            except ValidationError:
                return HttpResponse("There was an error")
        return super().get(request, *args, **kwargs)


class LibraryDetail(LoginRequiredMixin, generic.DetailView):
    template_name = 'books/library_detail.html'
    context_object_name = 'library_detail'
    model = Library

    ORDERING_OPTIONS = {
        'title': 'book__title',
        'author': 'book__authors',
        'shelf': 'location__name',
        'publisher': 'book__publisher',
        'pagecount': 'book__page_count',
        'isbn': 'book__isbn13',
        'language': 'book__language',
        'addeddate': 'book__added_date',
    }
    DEFAULT_PAGE_SIZE = 10

    def get_object(self, queryset=None):
        obj = super().get_object(queryset=queryset)
        if obj.owner != self.request.user:
            raise PermissionDenied()
        return obj

    def get_book_list(self):
        try:
            if self.request.GET.get('shelf') is not None:
                shelf = int(self.request.GET.get('shelf'))
            else:
                shelf = None
        except ValueError:
            shelf = None
        book_list = BookCopy.objects.filter(location__library=self.object)
        if self.request.GET.get('q') is not None and self.request.GET.get('q') != '':
            q = self.request.GET.get('q')
            similarity = TrigramSimilarity('book__title', q) + TrigramSimilarity('book__authors', q)
            book_list = book_list.annotate(similarity=similarity).filter(similarity__gt=0.1).order_by('-similarity')
        ordering_tag = self.request.GET.get('ordering')
        if ordering_tag is not None:
            if ordering_tag in self.ORDERING_OPTIONS:
                ordering = self.ORDERING_OPTIONS[ordering_tag]
                book_list = book_list.order_by(ordering)
            elif len(ordering_tag) > 1 and ordering_tag[0] == '-' and ordering_tag[1:] in self.ORDERING_OPTIONS:
                ordering = '-' + self.ORDERING_OPTIONS[self.request.GET.get('ordering')[1:]]
                book_list = book_list.order_by(ordering)
        if shelf is not None:
            book_list = book_list.filter(location__pk=shelf)
        return book_list.select_related('book', 'location')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        book_list = self.get_book_list()
        if self.request.GET.get('page_size') is None:
            page_size = self.DEFAULT_PAGE_SIZE
        else:
            try:
                page_size = int(self.request.GET.get('page_size'))
            except ValueError:
                page_size = self.DEFAULT_PAGE_SIZE

        pag = Paginator(book_list, page_size)
        shelf_list = Shelf.objects.filter(library=self.object).order_by('name')
        shelf_list_pairs = [(s.pk, s.name) for s in shelf_list]
        if self.request.GET.get('page') is None:
            page_number = 1
        else:
            try:
                page_number = int(self.request.GET.get('page'))
            except ValueError:
                page_number = 1
        if page_number < 1:
            page_number = 1
        if page_number > pag.num_pages:
            page_number = pag.num_pages
        context['page_obj'] = pag.page(page_number)
        context['book_list'] = book_list
        context['shelf_list'] = shelf_list
        context['form_book'] = NewBookForm(shelf_list=shelf_list_pairs)
        context['form_shelf'] = NewShelfForm()
        return context


@login_required
def book_add(request, pk):
    next = request.POST.get('next', '/')
    urlargs = '?' + urlencode({i: request.POST[i] for i in ['q', 'shelf', 'ordering'] if i in request.POST})
    library = get_object_or_404(Library, pk=pk)
    if request.method == 'POST':
        if library.owner != request.user:
            raise PermissionDenied()
        shelf_list_pairs = [(s.pk, s.name) for s in Shelf.objects.filter(library=library).order_by('name')]
        form_book = NewBookForm(request.POST, shelf_list=shelf_list_pairs)
        if form_book.is_valid():
            form_book.full_clean()
            isbn = form_book.cleaned_data['isbn']
            location = Shelf.objects.get(pk=form_book.cleaned_data['location'])
            if location.library != library:
                raise PermissionDenied()
            try:
                book_copy = BookCopy.create(isbn=isbn, location=location)
                book_copy.full_clean()
                book_copy.save()
                messages.success(request, "Book '{}' saved".format(book_copy.book.title))
                return HttpResponseRedirect(next + urlargs)
            except ISBNLookupFail:
                messages.error(request, 'Book with ISBN {} not found'.format(isbn))
                return HttpResponse(next + urlargs)
            except Exception as e:
                messages.error(request, "Error: {}".format(e))
        else:
            if len(shelf_list_pairs) == 0:
                messages.error(request, "You need to add a shelf before adding a book.")
                return HttpResponse(next + urlargs)
            messages.error(request, "Form not valid")
    return HttpResponse(next + urlargs)


@login_required
def shelf_add(request, pk):
    next = request.POST.get('next', '/')
    urlargs = '?' + urlencode({i: request.POST[i] for i in ['q', 'shelf', 'ordering'] if i in request.POST})
    library = get_object_or_404(Library, pk=pk)
    if request.method == 'POST':
        if library.owner != request.user:
            raise PermissionDenied()
        form_shelf = NewShelfForm(request.POST)
        if form_shelf.is_valid():
            name = form_shelf.cleaned_data['name']
            try:
                shelf = Shelf(name=name, library=library)
                shelf.full_clean()
                shelf.save()
                messages.success(request, "Shelf '{}' added".format(name))
                return HttpResponseRedirect(next + urlargs)
            except Exception as e:
                messages.error(request, "Error: {}".format(e))
                return HttpResponse(next + urlargs)
        else:
            messages.error(request, "Form not valid")
    return HttpResponse(next + urlargs)


class BookDetail(generic.DetailView):
    template_name = 'books/book_detail.html'
    context_object_name = 'book_detail'
    model = Book

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            copies = BookCopy.objects.filter(book=self.object, location__library__owner=self.request.user) \
                .select_related('location', 'location__library')
            context['copy_list'] = [bc.location for bc in copies]
            context['counted_copy_list'] = {i:context['copy_list'].count(i) for i in context['copy_list']}
        return context


class BookCopyDelete(LoginRequiredMixin, generic.DeleteView):
    model = BookCopy

    def get_success_url(self):
        return self.request.POST.get('next')

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.location.library.owner != request.user:
            raise PermissionDenied()
        return super(BookCopyDelete, self).dispatch(request, *args, **kwargs)


@login_required
def copy_move(request, pk):
    bc = get_object_or_404(BookCopy, pk=pk)
    if bc.location.library.owner != request.user:
        raise PermissionDenied()
    dest = get_object_or_404(Shelf, pk=request.POST.get('destination'))
    if dest.library.owner != request.user:
        raise PermissionDenied()
    if request.method == 'POST':
        try:
            bc.location = dest
            bc.save()
        except Exception as e:
            messages.error(request, "Error: {}".format(e))
    return HttpResponseRedirect(request.POST.get('next', '/'))


class ShelfDelete(LoginRequiredMixin, generic.DeleteView):
    model = Shelf

    def get_success_url(self):
        return self.request.GET.get('next')

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        contents = BookCopy.objects.filter(location=self.object)
        if len(contents) == 0:
            return self.post(request, *args, **kwargs)
        return super(ShelfDelete, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.library.owner != request.user:
            raise PermissionDenied()
        return super(ShelfDelete, self).post(request, *args, **kwargs)


@login_required
def shelf_rename(request, pk):
    s = get_object_or_404(Shelf, pk=pk)
    if s.library.owner != request.user:
        raise PermissionDenied()
    if request.method == 'POST':
        try:
            s.name = request.POST.get('name')
            s.save()
        except Exception as e:
            messages.error(request, "Error: {}".format(e))
    return HttpResponseRedirect(request.POST.get('next', '/'))


class BookUpdate(PermissionRequiredMixin, SuccessMessageMixin, generic.UpdateView):
    permission_required = 'books.change_book'
    template_name = 'books/book_update.html'
    model = Book
    fields = ['title', 'authors', 'publisher', 'publication_year', 'language', 'page_count', 'description', 'thumbnail']
    success_message = 'Book updated'

    def get_success_url(self):
        return reverse('books:book_update', args=[self.object.pk])
