# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.postgres.operations import TrigramExtension, HStoreExtension

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('books', '0001_initial'),
    ]

    operations = [
        HStoreExtension(),
        TrigramExtension(),
    ]
