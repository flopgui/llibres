# from django.conf import settings
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
import isbnlib
import requests


class Library(models.Model):
    name = models.CharField(max_length=30)
    added_date = models.DateTimeField(default=timezone.now)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, models.CASCADE)

    class Meta:
        unique_together = ['name', 'owner']

    def __str__(self):
        return self.name


class Shelf(models.Model):
    library = models.ForeignKey(Library, models.CASCADE)
    name = models.CharField(max_length=30)
    added_date = models.DateTimeField(default=timezone.now)

    class Meta:
        unique_together = ['library', 'name']

    def __str__(self):
        return self.name + " at " + self.library.name


class ISBNLookupFail(Exception):
    pass


def validate_isbn13(isbn):
    if isbn <= 0:
        raise ValidationError('The value must be a positive number')
    if isbn < 9780000000000 or isbn >= 9800000000000:
        raise ValidationError('Thie ISBN is not valid')
    si = str(isbn)
    s = sum(int(i) for i in si[::2]) + 3 * sum(int(i) for i in si[1::2])
    if s % 10 != 0:
        raise ValidationError('The ISBN is not valid')


class SeparatedValuesField(models.TextField):

    def __init__(self, *args, **kwargs):
        self.separator = kwargs.pop('separator', ',')
        super(SeparatedValuesField, self).__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        if self.separator != ',':
            kwargs['separator'] = self.separator
        return name, path, args, kwargs

    def from_db_value(self, value, expression, connection):
        return self.to_python(value)

    def to_python(self, value):
        if value is None:
            return
        if isinstance(value, list):
            return value
        return value.split(self.separator)

    def get_db_prep_value(self, value, connection, prepared=False):
        if not value:
            return
        assert(isinstance(value, list) or isinstance(value, tuple))
        return self.separator.join([s for s in value])

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        return self.get_db_prep_value(value)


class Book(models.Model):
    isbn13 = models.DecimalField(max_digits=13, decimal_places=0,  validators=[validate_isbn13], primary_key=True)
    title = models.CharField(max_length=200)
    authors = SeparatedValuesField(separator=';')
    description = models.TextField(null=True, blank=True)
    language = models.CharField(max_length=10, null=True, blank=True)
    page_count = models.DecimalField(max_digits=6, decimal_places=0, null=True, blank=True)
    publisher = models.CharField(max_length=200, null=True, blank=True)
    publication_year = models.DecimalField(max_digits=4, decimal_places=0, null=True, blank=True)
    thumbnail = models.CharField(max_length=200, null=True, blank=True)
    added_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.title) + " (" + self.isbn13 + ")"

    def lookup_request(isbn):
        url = 'https://en.wikipedia.org/api/rest_v1/data/citation/mediawiki/{}'.format(isbn)
        try:
            resp = requests.get(url).json()[0]
            data = {}
            data['Title'] = resp['title']
            data['Authors'] = [a[1] for a in resp['author']]
            if 'numPages' in resp:
                try:
                    data['NumPages'] = resp['NumPages'].split(' ')[0]
                except Exception:
                    pass
            if 'publisher' in resp:
                try:
                    data['Publisher'] = resp['publisher']
                except Exception:
                    pass
            if 'date' in resp:
                try:
                    int(resp['date'])
                    data['Year'] = resp['date']
                except Exception:
                    pass
            return data
        except Exception:
            pass
        try:
            data = {}
            for service in ['wiki', 'goob', 'openl']:
                try:
                    data = isbnlib.meta(str(isbn), service=service)
                    print(service)
                    print(data)
                    if data != {}:
                        break
                except Exception:
                    continue
            if data == {}:
                return None
            return data
        except Exception:
            return None

    def lookup(self):
        data = Book.lookup_request(self.isbn13)
        if data is None:
            raise ISBNLookupFail('Data not found in ISBN servers')
        try:
            self.title = data['Title']
            if data['Authors'] != []:
                self.authors = [a.strip('., ') for a in data['Authors']]
            else:
                self.authors = ['???']
            if 'Language' in data and data['Language'] != '':
                self.language = data['Language']
            if 'Publisher' in data and data['Publisher'] != '':
                self.publisher = data['Publisher']
            if 'Year' in data and data['Year'] != '':
                self.publication_year = int(data['Year'])
            if 'NumPages' in data and data['NumPages'] != '':
                self.page_count = int(data['NumPages'])
        except Exception:
            raise ISBNLookupFail('Error processing book data')
        desc = isbnlib.desc(str(self.isbn13))
        if desc is not None and desc != '':
            self.description = desc
        thumb = isbnlib.cover(str(self.isbn13))
        if 'thumbnail' in thumb and thumb['thumbnail'] != '':
            self.thumbnail = thumb['thumbnail']


class BookCopy(models.Model):
    book = models.ForeignKey(Book, models.PROTECT)
    added_date = models.DateTimeField(default=timezone.now)
    location = models.ForeignKey(Shelf, models.CASCADE)

    def __str__(self):
        return str(self.book.title) + " (" + str(self.book.isbn13) + ") at " + \
            self.location.name + ", " + self.location.library.name

    def create(isbn, location):
        book = Book.objects.filter(isbn13=isbn).first()
        if book is None:
            book = Book(isbn13=isbn)
            book.lookup()
            book.full_clean()
            book.save()
        return BookCopy(book=book, location=location)
